package com.example.devasr.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.example.devasr.view.GeneralFragment;
import com.example.devasr.view.IsolatedFragment;

/**
 * Created by nalioesyn on 09,July,2018
 */
public class ASRViewPager extends SmartFragmentStatePagerAdapter {
    FragmentManager fragmentManager;

    public ASRViewPager(FragmentManager fm) {
        super(fm);
        fragmentManager = fm;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new GeneralFragment();
            case 1:
                return new IsolatedFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
