package com.example.devasr;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HTTPPost {

    public static final String ERROR_CODE_TIME_OUT = "408";
    public static final String ERROR_CODE_UNKNOW_HOST = "404";
    SharedPreferences sp;
    private boolean proxy = false;
    private String proxy_host = "192.168.0.100";
    private int proxy_port = 8888;
    String[] mcc;
    String[] mnc;
    String token = "";

    public String sendJSON(JSONObject jo, String url) {

        String result = null;
        try {

            SSLContext sc;
            sc = SSLContext.getInstance("TLS");
            sc.init(null, null, new java.security.SecureRandom());
            URL object = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) object.openConnection();
            con.setSSLSocketFactory(sc.getSocketFactory());
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(jo.toString());
            wr.flush();

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                result = sb.toString();
            } else {
                System.out.println(con.getResponseMessage());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return result;
    }


    public String getHttpData(String url) {

        String result = "";

        try {

            // Create the SSL connection
//            SSLContext sc;
//            sc = SSLContext.getInstance("TLS");
//            sc.init(null, null, new java.security.SecureRandom());

            URL object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
//            con.setSSLSocketFactory(sc.getSocketFactory());
            con.setRequestMethod("GET");

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                result = sb.toString();
            } else {
                System.out.println(con.getResponseMessage());
            }
        } catch (IncompatibleClassChangeError e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public String toJSONString(String result) {
        if (result.startsWith("("))
            return result.substring(1);
        if (!result.startsWith("{")) {
            int index = result.indexOf("{");
            return result.substring(index);
        } else
            return result;
    }

    public List<String> processAsrAsynchronous(Context context, File file, String url, boolean fromVoice, TemplateItem item) {
        List<String> responses = new ArrayList<>();
        try {

            sp = context.getSharedPreferences("casts", Context.MODE_PRIVATE);
            int sim1_mcc = sp.getInt("mccslot1", 0);
            int sim1_mnc = sp.getInt("mncslot1", 0);
            int sim2_mcc = sp.getInt("mccslot2", 0);
            int sim2_mnc = sp.getInt("mncslot2", 0);
            if (sim2_mnc != 0) {
                mcc = new String[2];
                mcc[0] = Integer.toString(sim1_mcc);
                mcc[1] = Integer.toString(sim2_mcc);
                mnc = new String[2];
                mnc[0] = Integer.toString(sim1_mnc);
                mnc[1] = Integer.toString(sim2_mnc);
            } else {
                mcc = new String[1];
                mcc[0] = Integer.toString(sim1_mcc);
                mnc = new String[1];
                mnc[0] = Integer.toString(sim1_mnc);
            }

            try {
                String mDeviceID = Util.getUuid(context);
                long time = System.currentTimeMillis();
                String jo = mDeviceID + ":" + time + "";
                String ENCRYPTION_KEY = "A$rEnCK3y#";
                byte[] input = jo.toString().getBytes("utf-8");
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] thedigest = md.digest(ENCRYPTION_KEY.getBytes("UTF-8"));
                SecretKeySpec skc = new SecretKeySpec(thedigest, "AES/ECB/PKCS5Padding");
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, skc);
                byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
                int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
                ctLength += cipher.doFinal(cipherText, ctLength);
                token = Base64.encodeToString(cipherText, Base64.DEFAULT);

            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
                ;
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (ShortBufferException ex) {
                ex.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException ex) {
                ex.printStackTrace();
            }

            final MediaType MEDIA_TYPE = MediaType.parse("audio/wav");
            RequestBody requestBody;

            if (fromVoice) {
                if (mnc.length > 1) {
                    requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("device_id", Util.getUuid(context))
                            .addFormDataPart("mcc[" + 0 + "]", mcc[0])
                            .addFormDataPart("mcc[" + 1 + "]", mcc[1])
                            .addFormDataPart("udid",Util.getUuid(context))
                            .addFormDataPart("mnc[" + 0 + "]", mnc[0])
                            .addFormDataPart("mnc[" + 1 + "]", mnc[1])
                            .addFormDataPart("token", token.trim())
                            .addFormDataPart("audio", file.getName(), RequestBody.create(MEDIA_TYPE, file))
                            .build();

                } else {

                    requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("device_id", Util.getUuid(context))
                            .addFormDataPart("mcc[" + 0 + "]", mcc[0])
                            .addFormDataPart("mnc[" + 0 + "]", mnc[0])
                            .addFormDataPart("udid",Util.getUuid(context))
                            .addFormDataPart("token", token.trim())
                            .addFormDataPart("audio", file.getName(), RequestBody.create(MEDIA_TYPE, file))
                            .build();

                }

            } else {
                if (mnc.length > 1) {
                    requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("device_id", Util.getUuid(context))
                            .addFormDataPart("mcc[" + 0 + "]", mcc[0])
                            .addFormDataPart("mcc[" + 1 + "]", mcc[1])
                            .addFormDataPart("mnc[" + 0 + "]", mnc[0])
                            .addFormDataPart("mnc[" + 1 + "]", mnc[1])
                            .addFormDataPart("udid",Util.getUuid(context))
                            .addFormDataPart("category", item.getCategory())
                            .addFormDataPart("token", token.trim())
                            .addFormDataPart("sub_category", item.getSub_category())
                            .addFormDataPart("fun", item.getFun())
                            .build();
                } else {
                    requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("device_id", Util.getUuid(context))
                            .addFormDataPart("mcc[" + 0 + "]", mcc[0])
                            .addFormDataPart("mnc[" + 0 + "]", mnc[0])
                            .addFormDataPart("udid",Util.getUuid(context))
                            .addFormDataPart("category", item.getCategory())
                            .addFormDataPart("token", token.trim())
                            .addFormDataPart("sub_category", item.getSub_category())
                            .addFormDataPart("fun", item.getFun())
                            .build();
                }
            }

            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .readTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .build();

            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                responses.add(response.body().string());
                responses.add((response.receivedResponseAtMillis() - response.sentRequestAtMillis()) + "");
                //track for long logs
//                int maxLogSize = 1000;
//                for(int i = 0; i <= responses.length() / maxLogSize; i++) {
//                    int start = i * maxLogSize;
//                    int end = (i+1) * maxLogSize;
//                    end = end > responses.length() ? responses.length() : end;
//                }
                return responses;

            } else {
                responses.add("လုပ်ဆောင်မှု မအောင်မြင်ပါသဖြင့် ထပ်မံ ကြိုးစား ကြည့်ပါ။");
                return responses;
            }

        } catch (Exception e) {
            responses.add("လုပ်ဆောင်မှု မအောင်မြင်ပါသဖြင့် ထပ်မံ ကြိုးစား ကြည့်ပါ။");
            return responses;
        }
    }


}
