package com.example.devasr.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.example.devasr.AudioView;
import com.example.devasr.Converter;
import com.example.devasr.FontDetect;
import com.example.devasr.HTTPPost;
import com.example.devasr.MainActivity;
import com.example.devasr.R;
import com.example.devasr.Util;
import com.example.devasr.recorder.AbstractRecorder;
import com.example.devasr.recorder.AudioChunk;
import com.example.devasr.recorder.AudioRecordConfig;
import com.example.devasr.recorder.OmRecorder;
import com.example.devasr.recorder.PullTransport;
import com.example.devasr.recorder.PullableSource;
import com.example.devasr.recorder.Recorder;
import com.example.devasr.recorder.WriteAction;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GeneralFragment extends Fragment implements AudioView.OnRecordListener{

    private AudioView recordView;
    public boolean mIsRecording = false;
    private Handler mHandler;
    Recorder recorder;
    Runnable mCallBacks;
    private String mFileName;
    private CountDownTimer timer;
    private int maxInterval = 10000;
    private ProgressBar clipProgress;
    private boolean isBeginAccessToApi = false;
    private boolean isOverClip = false;
    final int[] progress = {0};
    private boolean isUnicode;
    private String content;
    private SharedPreferences prefs;
    private final long[] length = new long[1];
    private View view;
    private TextView title;
    private TextView logOne;
    private TextView logTwo;
    private TextView logThree;
    private TextView logFour;
    private TextView logFive;

    private TextView msOne;
    private TextView msTwo;
    private TextView msThree;
    private TextView msFour;
    private TextView msFive;

    private EditText eOne;
    private EditText eTwo;
    private EditText eThree;
    private EditText eFour;
    private EditText eFive;
    int apiCount = 0;
    private @SuppressLint("StaticFieldLeak")
    RequestASRAsync async;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.general_fragment,container,false);
        title = view.findViewById(R.id.txtTitle);
        eOne = view.findViewById(R.id.one);
        eTwo = view.findViewById(R.id.two);
        eThree = view.findViewById(R.id.three);
        eFour = view.findViewById(R.id.four);
        eFive = view.findViewById(R.id.five);
        recordView = view.findViewById(R.id.recordView);
        clipProgress = view.findViewById(R.id.clip_progress);
        logOne = view.findViewById(R.id.logOne);
        logTwo = view.findViewById(R.id.logTwo);
        logThree = view.findViewById(R.id.logThree);
        logFour = view.findViewById(R.id.logfour);
        logFive = view.findViewById(R.id.logFive);
        msOne = view.findViewById(R.id.onems);
        msTwo = view.findViewById(R.id.twoms);
        msThree = view.findViewById(R.id.threems);
        msFour = view.findViewById(R.id.fourems);
        msFive = view.findViewById(R.id.fivems);
        clipProgress.setProgress(0);
        mHandler = new Handler(Looper.getMainLooper());

        if (FontDetect.isUnicode()){
            isUnicode = true;
        }
        if (isUnicode) {
            content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
        } else {
            content = "ဖိထား ၍ စကားေျပာပါ";
        }

        title.setText(content);
        initRecording();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private boolean validateMicAvailability() {
        Boolean available = true;
        AudioRecord recorder =
                new AudioRecord(MediaRecorder.AudioSource.MIC, 44100,
                        AudioFormat.CHANNEL_IN_MONO,
                        AudioFormat.ENCODING_DEFAULT, 44100);
        try {
            if (recorder.getRecordingState() != AudioRecord.RECORDSTATE_STOPPED) {
                available = false;
            }

            recorder.startRecording();
            if (recorder.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
                recorder.stop();
                available = false;
            }
            recorder.stop();
        } finally {
            recorder.release();
            recorder = null;
        }

        return available;
    }

    private void initRecording() {
        if (validateMicAvailability()) {
            recordView.setOnRecordListener(this);
        } else {
            if (recorder != null) {
                try {
                    recorder.stopRecording();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            recordView.setOnRecordListener(this);
        }
        setupRecorder();
    }

    public void setupRecorder() {
        timer = new CountDownTimer(maxInterval, 1000) {
            @Override
            public void onTick(long l) {
                clipProgress.setProgress(++progress[0]);
                length[0] = l;
            }

            @Override
            public void onFinish() {
                if (recorder != null) {
                    mIsRecording = false;
                    isOverClip = true;
                    progress[0] = 0;
                    Toast.makeText(getContext(), "Recording voice clip must be within 10 seconds.", Toast.LENGTH_SHORT).show();
                    try {
                        if (!mIsRecording) {
                            clipProgress.setProgress(0);
                            if (isUnicode) {
                                content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
                            } else {
                                content = "ဖိထား ၍ စကားေျပာပါ";
                            }
                            title.setText(content);
                            recordView.animateRadius(0);

                            if (isFilePresent(mFileName + ".wav")) {
                                deleteExistingFile(mFileName + ".wav");
                            }
                        }
                        recorder.stopRecording();
                        recorder = null;
                        if (timer != null) {
                            timer.cancel();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        mCallBacks = new Runnable() {
            @Override
            public void run() {
                if (recorder == null) {
                    recorder = initRecorder();
                }
                timer.start();
                recorder.startRecording();
                isOverClip = false;
            }
        };
    }


    public boolean isFilePresent(String fileName) {
        File file = new File(getContext().getFilesDir().getAbsolutePath(), "/voice/" + fileName);
        return file.exists();
    }

    public File getFile(Context context, String fileName) {
        File file = new File(context.getFilesDir().getAbsolutePath(), "/voice/" + fileName);
        if (file.exists()) {
            return file;
        } else {
            return null;
        }
    }

    public void deleteExistingFile(String fileName) {
        File file = new File(getContext().getFilesDir().getAbsolutePath() + "/voice/" + fileName);
        if (file != null) {
            if (file.exists()) {
                boolean deletedFile = file.delete();
            }
        }
    }

    private PullableSource mic() {
        return new PullableSource.NoiseSuppressor(
                new PullableSource.Default(
                        new AudioRecordConfig.Default(
                                MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                                AudioFormat.CHANNEL_IN_MONO, 16000
                        )
                )
        );
    }


    @NonNull
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private File file() {
        File file = new File(getContext().getFilesDir().getAbsolutePath(), "/voice");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            mFileName = cryptoHashByID("WAV_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + "_" + Util.getUuid(getContext()));
        } catch (NoSuchAlgorithmException nse) {
            nse.printStackTrace();
        }
        return new File(getContext().getFilesDir().getAbsolutePath(), "/voice/" + mFileName + ".wav");
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    public String cryptoHashByID(String raw) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(raw.getBytes(StandardCharsets.UTF_16));
        byte[] digest = md.digest();

        return String.format("%064x", new BigInteger(1, digest));
    }

    public Recorder initRecorder() {
        recorder = OmRecorder.wav(
                new PullTransport.Noise(mic(),
                        new PullTransport.OnAudioChunkPulledListener() {
                            @Override
                            public void onAudioChunkPulled(final AudioChunk audioChunk) {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!isBeginAccessToApi) {
                                            if (isUnicode) {
                                                content = Converter.zg12uni51("နားေထာင္ေနပါသည္...");
                                            } else {
                                                content = "နားေထာင္ေနပါသည္...";
                                            }
                                            title.setText(content);
                                        }
                                        if (!mIsRecording) {
                                            clipProgress.setProgress(0);
                                            if (!isBeginAccessToApi) {
                                                if (isUnicode) {
                                                    content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
                                                } else {
                                                    content = "ဖိထား ၍ စကားေျပာပါ";
                                                }
                                                title.setText(content);
                                            }
                                            recordView.animateRadius(0);
                                        }

                                        if (mIsRecording) {
                                            mHandler.postDelayed(this, 50);
                                        }
                                    }
                                });
                            }
                        },
                        new WriteAction.Default(),
                        new Recorder.OnSilenceListener() {
                            @Override
                            public void onSilence(long silenceTime) {
                            }
                        }, 500
                ), file(), new AbstractRecorder.OnFinishOutputStream() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void oNFinish(boolean isFinish) {
                        if (length[0] < 9000) {
                            if (isFinish && getFile(getContext(), mFileName + ".wav") != null && !isOverClip) {
                                async = getAsync();
                                async.execute("https://elb.baganasr.com/node/api/v1/get-text");
                            }
                        } else {
                            Toast.makeText(getContext(), "Recording voice clip length is too short.Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
        return recorder;
    }
    private RequestASRAsync getAsync() {

        @SuppressLint("StaticFieldLeak") RequestASRAsync asrAsync = new RequestASRAsync(getContext(), getFile(getContext(), mFileName + ".wav")) {
            @Override
            protected void onPreExecute() {
                isBeginAccessToApi = true;
                if (isUnicode) {
                    content = Converter.zg12uni51("လုပ္္ေဆာင္ေနပါသည္။ ေခတၱေစာင့္ပါ။");
                } else {
                    content = "လုပ္္ေဆာင္ေနပါသည္။ ေခတၱေစာင့္ပါ။";
                }
                title.setText(content);
                recordView.setEnabled(false);

            }

            @Override
            protected void onPostExecute(List<String> s) {
                try {
                    JSONObject object = new JSONObject(s.get(0));
                    requestAsrModel(object, s);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), s.get(0), Toast.LENGTH_SHORT).show();
                }
            }
        };

        return asrAsync;
    }

    private void requestAsrModel(JSONObject object, List<String> raw) {
        String response = "";
        try {
            if (apiCount == 0) {
                response = object.getString("text");
            } else {
                response = object.getString("value");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Something wrong!", Toast.LENGTH_SHORT).show();
        }

        if (apiCount == 0) {
            eOne.setText(response);
            logOne.setText(raw.get(0));
            msOne.setText("( " + raw.get(1) + " ms )");
            ++apiCount;
            if (async.getStatus() == AsyncTask.Status.RUNNING) {
                async.cancel(true);
                async = null;
                async = getAsync();
                async.execute("https://elb.baganasr.com:8443/node/api/v1/get-text-dev-2");
            }
        } else if (apiCount == 1) {
            eTwo.setText(response);
            logTwo.setText(raw.get(0));
            msTwo.setText("( " + raw.get(1) + " ms )");
            ++apiCount;
            if (async.getStatus() == AsyncTask.Status.RUNNING) {
                async.cancel(true);
                async = null;
                async = getAsync();
                async.execute("https://elb.baganasr.com:8443/node/api/v1/get-text-dev-3");
            }
        } else if (apiCount == 2) {
            eThree.setText(response);
            logThree.setText(raw.get(0));
            msThree.setText("( " + raw.get(1) + " ms )");
            ++apiCount;
            if (async.getStatus() == AsyncTask.Status.RUNNING) {
                async.cancel(true);
                async = null;
                async = getAsync();
                async.execute("https://elb.baganasr.com:8443/node/api/v1/get-text-dev-4");
            }
        } else if (apiCount == 3) {
            eFour.setText(response);
            logFour.setText(raw.get(0));
            msFour.setText("( " + raw.get(1) + " ms )");
            ++apiCount;
            if (async.getStatus() == AsyncTask.Status.RUNNING) {
                async.cancel(true);
                async = null;
                async = getAsync();
                async.execute("https://elb.baganasr.com:8443/node/api/v1/get-text-dev-5");
            }
        } else {
            isBeginAccessToApi = false;
            if (isUnicode) {
                content = Converter.zg12uni51("ဖိထား ၍ စကားေျပာပါ");
            } else {
                content = "ဖိထား ၍ စကားေျပာပါ";
            }
            title.setText(content);
            recordView.setEnabled(true);
            eFive.setText(response);
            msFive.setText("( " + raw.get(1) + " ms )");
            logFive.setText(raw.get(0));
            apiCount = 0;
            if (isFilePresent(mFileName + ".wav")) {
                deleteExistingFile(mFileName + ".wav");
            }
        }
    }

    public static class RequestASRAsync extends AsyncTask<String, Void, List<String>> {
        File mFile;
        Context context;
        List<String> result = new ArrayList<>();

        public RequestASRAsync(Context context, File file) {
            mFile = file;
            this.context = context;
        }

        @Override
        protected List<String> doInBackground(String... strings) {
            try {
                HTTPPost post = new HTTPPost();
                result = post.processAsrAsynchronous(context, mFile, strings[0], true, null);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
                result.add("Connection timeout!!!Please try again...");
                return result;
            }
        }
    }


    @Override
    public void onRecordStart() {
        try {
            mIsRecording = true;
            mHandler.post(mCallBacks);
        } catch (Exception e) {
            Toast.makeText(getContext(), "MediaRecorder prepare failed!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void resetAudio() {
        mIsRecording = false;
        progress[0] = 0;
        if (recorder != null) {
            try {
                recorder.stopRecording();
                recorder = null;
                if (timer != null) {
                    timer.cancel();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRecordFinish() {
        mIsRecording = false;
        progress[0] = 0;
        if (recorder != null) {
            try {
                recorder.stopRecording();
                recorder = null;
                if (timer != null) {
                    timer.cancel();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
