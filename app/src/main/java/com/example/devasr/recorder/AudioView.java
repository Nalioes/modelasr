package com.example.devasr.recorder;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.example.devasr.R;

/**
 * Created by nalioesyn on 30,October,2018
 */
public class AudioView extends View {

    private static final int STATE_NORMAL = 0;
    private static final int STATE_PRESSED = 1;
    private static final int STATE_RECORDING = 2;

    private int mState = STATE_NORMAL;
    private boolean mIsRecording = false;

    private AnimatorSet mAnimatorSet = new AnimatorSet();
    private OnRecordListener mOnRecordListener;
    private Bitmap mNormalBitmap;
    private Bitmap mRecordingBitmap;
    private Bitmap mPressedBitmap;
    private Paint mPaint;
    private float mMaxRadius;
    private float mMinRadius;
    private float mCurrentRadius;
    boolean isLongPressed = false;

    public AudioView(Context context) {
        super(context);
        initView();
    }

    public AudioView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AudioView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    OnTouchListener actionListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                return false;
            }

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (isLongPressed) {
                    isLongPressed = false;
                    if (mIsRecording) {
                        mState = STATE_NORMAL;
                        if (mOnRecordListener != null) {
                            mOnRecordListener.onRecordFinish();
                            animateRadius(0);
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "Press hold to send a voice clip.", Toast.LENGTH_SHORT).show();
                }
                mIsRecording = !mIsRecording;
                invalidate();
                return AudioView.super.onTouchEvent(event);
            }
            return AudioView.super.onTouchEvent(event);
        }
    };
    private OnLongClickListener longClickListener = new OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            isLongPressed = true;
            if (!mIsRecording) {
                mState = STATE_RECORDING;
                if (mOnRecordListener != null) {
                    mOnRecordListener.onRecordStart();
                    callFallbackAction(true);
                    mIsRecording = true;
                }
            }
            invalidate();
            return true;
        }
    };

    public AudioView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = canvas.getWidth();
        int height = canvas.getHeight();

        if (mCurrentRadius > mMinRadius) {
            canvas.drawCircle(width / 2, height / 2, mCurrentRadius, mPaint);
        }
        switch (mState) {
            case STATE_NORMAL:
                canvas.drawBitmap(mNormalBitmap, (width - mNormalBitmap.getWidth()) / 2, (height - mNormalBitmap.getHeight()) / 2, mPaint);
                break;
            case STATE_PRESSED:
                canvas.drawBitmap(mPressedBitmap, (width - mPressedBitmap.getWidth()) / 2, (height - mPressedBitmap.getHeight()) / 2, mPaint);
                break;
            case STATE_RECORDING:
                canvas.drawBitmap(mRecordingBitmap, (width - mRecordingBitmap.getWidth()) / 2, (height - mRecordingBitmap.getHeight()) / 2, mPaint);
                break;
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mMaxRadius = Math.min(w, h) / 2;
    }

    public static int dp2px(Context context, int dp) {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        return px;
    }

    public void animateRadius(float radius) {
        if (radius <= mCurrentRadius) {
            return;
        }
        if (radius > mMaxRadius) {
            radius = mMaxRadius;
        } else if (radius < mMinRadius) {
            radius = mMinRadius;
        }
        if (radius == mCurrentRadius) {
            return;
        }
        if (mAnimatorSet.isRunning()) {
            mAnimatorSet.cancel();
        }

        mAnimatorSet.playSequentially(
                ObjectAnimator.ofFloat(this, "CurrentRadius", getCurrentRadius(), radius).setDuration(50),
                ObjectAnimator.ofFloat(this, "CurrentRadius", radius, mMinRadius).setDuration(600)
        );
        mAnimatorSet.start();
    }

    public void setCurrentRadius(float currentRadius) {
        mCurrentRadius = currentRadius;
        invalidate();
    }

    public float getCurrentRadius() {
        return mCurrentRadius;
    }

    public void callFallbackAction(boolean fallback) {
        setHapticFeedbackEnabled(fallback);
    }

    public void initView() {
        mNormalBitmap = getBitmapFromVectorDrawable(getContext(), R.drawable.normal_mic);
        mPressedBitmap = getBitmapFromVectorDrawable(getContext(), R.drawable.pressed_mic);
        mRecordingBitmap = getBitmapFromVectorDrawable(getContext(), R.drawable.record_mic);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.argb(255, 33, 150, 243));

        mMinRadius = dp2px(getContext(), 68) / 2;
        mCurrentRadius = mMinRadius;
        setOnLongClickListener(longClickListener);
        setOnTouchListener(actionListener);
    }

    @Override
    public void setHapticFeedbackEnabled(boolean hapticFeedbackEnabled) {
        super.setHapticFeedbackEnabled(false);
    }

    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                return super.onTouchEvent(event);
        }
        return super.onTouchEvent(event);
    }

    public void setOnRecordListener(OnRecordListener onRecordListener) {
        mOnRecordListener = onRecordListener;
    }

    public static interface OnRecordListener {
        void onRecordStart();

        void onRecordFinish();
    }
}
