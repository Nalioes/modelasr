package com.example.devasr;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by nls on 9/27/16.
 */

public class CompatTextView extends AppCompatTextView {
    private static Typeface mmTypeFace;

    public CompatTextView(Context context) {
        super(context);
        setMMFont(this);
    }

    public CompatTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setMMFont(this);
    }

    public CompatTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setMMFont(this);
    }


    public static void setMMFont(TextView view) {
        if (FontDetect.isUnicode()) {
            mmTypeFace = Typeface.createFromAsset(view.getContext().getAssets(), "fonts/pyidaungsu.ttf");
        }else{
            mmTypeFace = Typeface.createFromAsset(view.getContext().getAssets(), "fonts/zawgyi.ttf");
        }
        view.setTypeface(mmTypeFace);
    }

}
